require "http"
require "json"

# Contains objects to help work with zones and records using the  CloudFlare V4
# API.
module CloudFlare
  class_property uri do
    URI.parse("https://api.cloudflare.com")
  end

  class_property token do
    ENV.fetch("CLOUDFLARE_API_KEY")
  end

  class_property client : HTTP::Client do
    HTTP::Client.new(uri).tap do |c|
      c.before_request { |r| r.headers["Authorization"] = "Bearer #{token}" }
    end
  end

  # A response from /zones.
  class Zones
    include JSON::Serializable

    property success : Bool
    property errors : Array(String)
    property messages : Array(String)
    property result : Array(Zone)

    def self.fetch
      Zones.from_json(CloudFlare.client.get("/client/v4/zones").body)
    end
  end

  # A single zone.
  class Zone
    include JSON::Serializable

    property id : String
    property name : String

    def path
      "/client/v4/zones/#{id}/dns_records"
    end

    def records
      Records.from_json(CloudFlare.client.get(path).body)
    end
  end

  # A response from /zones/{id}/dns_records.
  class Records
    include JSON::Serializable

    property success : Bool
    property errors : Array(String)
    property messages : Array(String)
    property result : Array(Record)
  end

  # A single record.
  class Record
    include JSON::Serializable
    property id : String
    property zone_id : String
    property zone_name : String
    property type : String
    property name : String
    property content : String
    property proxied : Bool
  end
end
