require "option_parser"
require "./cloudflare"

VERSION = "0.1.0"

OptionParser.parse(ARGV) do |o|
  o.banner = "Usage: ddns [options...] [command] [args...]"

  o.on("-h", "--help", "print help and exit") do
    puts o
    o.stop
  end

  o.on("-V", "--version", "print version info and exit") do
    puts "ddns v#{VERSION}"
    o.stop
  end

  o.on("zones", "list zones") do
    CloudFlare::Zones.fetch.result.each do |zone|
      printf("%s %s\n", zone.id, zone.name)
    end
  end

  o.on("records", "list records for a zone") do |*args|
    zones = CloudFlare::Zones.fetch.result
    o.unknown_args do |args, xargs|
      ids = args.map do |arg|
        zones.find do |z|
          [z.id, z.name].includes?(arg)
        end
      end.compact.each do |zone|
        zone.records.result.each do |record|
          printf("%s %-7s %s %s %s\n", record.id, record.type, record.proxied ? "*" : " ", record.name, record.content)
        end
      end
    end
  end
end

# cu3oirEukoqme1IDQWa0ufRRW-bCb6j9T6y-gDQx
